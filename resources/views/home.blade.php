<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Slashweb | Construcción</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="">

  <!-- ============ Google fonts ============ -->
  <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,400italic' rel='stylesheet' type='text/css'>

  <!-- ============ Bootstrap core CSS ============ -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- ============ Add custom CSS here ============ -->
  <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

  <!-- ============ Special CSS for Rainy ============ -->
  <style media="screen">#pattern {z-index:1;}#main-future {z-index:1001}#progress2{z-index:1}</style>

</head>
<body onload="run();">

<!-- ===== Loading ===== -->
<div class="globload">
  <div class="snaker animated fadeIn">
    <div class="circle1"></div>
    <div class="circle2"></div>
    <div class="circle3"></div>
    <div class="circle4"></div>
    <div class="circle5"></div>
    <div class="circle6"></div>
    <h6>LOADING</h6>
  </div>
</div>
<!-- ===== Loading ===== -->

<!-- ************** START / GLISS MAP ************** -->
<div id="map">
  <div class="container">
    <div class="col-xs-12 align-reduce">
      <a class="backoff">
        <div class="reducing">
          <i class="fa fa-compress"></i>
        </div>
      </a>
    </div>
  </div>
</div>
<!-- ************** END / GLISS MAP ************** -->

<div id="parent"><img id="background" alt="" src="img/bgcslash-min.jpg" /></div>

<div id="pattern"></div>

<h1 class="tlt">SlashwebMx</h1>

<!-- ************** START / MAIN FUTURE ************** -->
<section id="main-future" class="opacity-0">

  <div id="main-content">
    <div class="principal container">

      <h1 id="tit-main">Slashweb</h1>
      <span class="border"></span>
      <h4>Estamos en construcción, volveremos pronto</h4>

      <!-- ************** START / COUNTDOWN ************** -->

      <div id="countdown_dashboard">

        <div class="col-md-3 col-sm-3 col-xs-12 dash-glob">
          <div class="dash days_dash">
            <div class="digit">0</div>
            <div class="digit">0</div>
            <div class="digit">0</div>
            <span class="dash_title">Días</span>
          </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12 dash-glob">
          <div class="dash hours_dash">
            <div class="digit">0</div>
            <div class="digit">0</div>
            <span class="dash_title">Horas</span>
          </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12 dash-glob">
          <div class="dash minutes_dash">
            <div class="digit">0</div>
            <div class="digit">0</div>
            <span class="dash_title">Minutos</span>
          </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12 dash-glob">
          <div class="dash seconds_dash">
            <div class="digit">0</div>
            <div class="digit">0</div>
            <span class="dash_title">Segundos</span>
          </div>
        </div>

      </div>

      <!-- *************** END / COUNTDOWN *************** -->

    </div>



  </div> <!-- * END / Main content * -->

</section>
<!-- ************** END / MAIN FUTURE ************** -->

<div class="container">
  <div id="subscribe">
    <form method="POST" id="notifyMe" action="assets/php/notify-me.php">
      <div class="form-group">
        <div class="controls">
          <input type="text" id="mail-sub" name="email" placeholder="Write your email and stay tuned!" class="form-control email srequiredField">
        </div>
      </div>
      <button class="btn btn-lg submit">Subscribe</button>
    </form>
  </div>
</div>

<div class="container">
  <div id="close-map">
    <a id="close-map-top" class="fa-close-map">
      <i class="fa fa-angle-double-up"></i>
    </a>
  </div>
</div>

<audio id="beep-two" preload="auto">
  <source src="assets/sounds/glass-tone.mp3" />
</audio>

<audio src="assets/sounds/0595.mp3" autoplay loop ></audio>

<!-- ============ Javascript ============ -->
<script type="text/javascript" src="assets/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.lwtCountdown-1.0.js"></script>
<script type="text/javascript" src="assets/js/jquery.popupoverlay.js"></script>
<script type="text/javascript" src="assets/js/jquery.swipebox.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="assets/js/rainyday.js"></script>
<script type="text/javascript" src="assets/js/contact_me.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
<script type="text/javascript" src="assets/js/notifyMe.js"></script>
<script type="text/javascript" src="assets/js/jquery.lettering.js"></script>
<script type="text/javascript" src="assets/js/jquery.textillate.js"></script>
<script type="text/javascript" src="assets/js/future.js"></script>

<script>
    function run() {
        var image = document.getElementById('background');
        image.crossOrigin = 'anonymous';
        var engine = new RainyDay({
            element: 'background',
            blur: 0,
            opacity: 1,
            fps: 30
        });
        engine.trail = engine.TRAIL_SMUDGE;

        engine.rain([ [1, 0, 100], [3, 3, 1] ], 100);
    }

    $(window).load(function(){
        run();
    });
</script>

<script>
    ;( function( $ ) {

        $( '.swipebox' ).swipebox();

    } )( jQuery );
</script>

</body>
</html>